# How to install dependencies
Use poetry, venv or just install requirements.txt at bare system.

# How to add Webscenarios to Zabbix
1. Fill out the `list.json` by way of example from `list.example.json`.
2. Fill out `.env` by way of example from `.env.example`.
3. Run file `webscenario.py` in python interpreter with installed dependencies.
