from dataclasses import dataclass
import json


@dataclass
class Service:
    url: str = ''
    return_code: int = 200
    name: str = ''

class ServiceDecoder(json.JSONDecoder):
    def __init__(self, object_hook=None, *args, **kwargs):
        super().__init__(object_hook=self.object_hook, *args, **kwargs)

    def object_hook(self, o):
        decoded_service = Service(
            url=o.get('url'),
            name=o.get('name'),
            return_code=int(o.get('status_code')),
        )
        return decoded_service

def parse_list(filename: str) -> list[Service]:
    with open(filename, 'r') as f:
        json_obj = json.load(f, cls=ServiceDecoder)
    return json_obj