import os
import sys
from pyzabbix import ZabbixAPI, ZabbixAPIException

from service_class import parse_list

ZABBIX_URL = os.getenv("ZABBIX_URL")
HOST_NAME=os.getenv("HOST_NAME")
def trigger(zapi, host_id, name, tags):
    try:
        item = zapi.trigger.create(
            description=f'HTTPS check for {name}',
            expression=f"last(/{HOST_NAME}/web.test.fail[{name}],#2)>0",
            tags=tags,
            priority=5,
            recovery_mode=1,
            recovery_expression=f"max(/{HOST_NAME}/web.test.fail[{name}],#5)=0"
            )
    except ZabbixAPIException as e:
        print(e)
        sys.exit()
    print(item)


def scenario(zapi, host_id, url, name, service_tag):
    try:
        item = zapi.httptest.create(
            hostid=host_id,
            name=name,
            retries=3,
            tags=[service_tag],
            steps=[
            {
            "url": url,
            "name": name,
            "status_codes": "200",
            "no": 1
            }
            ])
    except ZabbixAPIException as e:
        print(e)
        sys.exit()
    print(item)

def main():
    zapi = ZabbixAPI(ZABBIX_URL)
    zapi.login(api_token=os.getenv("TOKEN"))
    hosts = zapi.host.get(filter={"host": HOST_NAME}, selectInterfaces=["interfaceid"])
    host_id = hosts[0]["hostid"]
    for v in parse_list("list.json"):
        name=f'HTTPS check for {v.name}'
        url=v.url
        service_tag={
            "tag": "service",
            "value": v.name
        }
        trigger_tags=[service_tag,{
            "tag": "opsgenie",
            "value": ""
        }]
        scenario(zapi, host_id, url, name, service_tag)
        trigger(zapi, host_id, name, trigger_tags)

if __name__ == "__main__":
    main()
   
